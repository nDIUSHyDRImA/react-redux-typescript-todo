import React, { useState } from 'react';
import { Redirect } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import { selectJwt, setAuthState } from '../auth/slice';
import { isLoggedIn, correctUsername, correctPassword } from '../auth';

export default function Login() {
  const [username, setUsername] = useState('foo');
  const [password, setPassword] = useState('bar');
  const [errorMessage, setErrorMessage] = useState('');
  const jwt = useSelector(selectJwt);
  const dispatch = useDispatch();

  const handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    if (!(correctUsername(username) && correctPassword(password))) {
      setErrorMessage('Invalid!');
      return;
    }
    dispatch(setAuthState({jwt: 'aaa'}));
  }

  return (
    <>
    {isLoggedIn(jwt) && <Redirect to="/" />}
    {!!errorMessage && (
      <p style={{ color: 'red' }}>{errorMessage}</p>
    )}
    <form onSubmit={e => handleSubmit(e)}>
      <input type="text" className="input" value={username} onChange={(e) => setUsername(e.currentTarget.value)} />
      <input type="password" className="input" value={password} onChange={(e) => setPassword(e.currentTarget.value)} />
      <button type="submit" className="button is-primary">Login</button>
    </form>
    </>
  )
}