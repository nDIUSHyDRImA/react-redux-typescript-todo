import React, { useState, useEffect } from "react";
import { resetAuthState, selectJwt } from "../../auth/slice";
import { useDispatch, useSelector } from "react-redux";
import { Link, useHistory } from "react-router-dom";
import { isLoggedIn } from "../../auth";

export default function Navbar() {
  const jwt = useSelector(selectJwt);
  const dispatch = useDispatch();
  const [isActive, setIsActive] = useState(false);
  const history = useHistory();
  const unListen = history.listen(() => setIsActive(false));
  // eslint-disable-next-line
  useEffect(() => unListen(), []);

  return (
    <nav
      className="navbar is-primary"
      role="navigation"
      aria-label="main navigation"
    >
      <div className="navbar-brand">
        <Link className="navbar-item" to="/">
          Todo App
        </Link>

        <span
          role="button"
          className="navbar-burger burger"
          aria-label="menu"
          aria-expanded="false"
          data-target="navbarBasicExample"
          onClick={() => setIsActive(!isActive)}
        >
          <span aria-hidden="true"></span>
          <span aria-hidden="true"></span>
          <span aria-hidden="true"></span>
        </span>
      </div>

      <div id="navbarBasicExample" className={`navbar-menu ${isActive ? 'is-active' : ''}`}>
        <div className="navbar-start"></div>

        <div className="navbar-end">
          <div className="navbar-item">
            <div className="buttons">
              {isLoggedIn(jwt) ? (
                <button
                  className={`button is-primary ${isActive ? '' : 'is-inverted is-outlined'}`}
                  onClick={() => dispatch(resetAuthState())}
                >
                  Log out
                </button>
              ) : (
                <Link
                  className={`button is-primary ${isActive ? '' : 'is-inverted is-outlined'}`}
                  to="/login"
                >
                  Log in
                </Link>
              )}
            </div>
          </div>
        </div>
      </div>
    </nav>
  );
}
