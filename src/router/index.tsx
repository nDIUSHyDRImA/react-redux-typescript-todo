import React, { ReactNode } from 'react';
import {
  Switch,
  Route,
  Redirect,
  HashRouter,
} from "react-router-dom";
import Index from '../App2';
import Login from '../login/index';
import Todo from '../todo/index';
import { useSelector } from 'react-redux';
import { selectJwt } from '../auth/slice';
import { isLoggedIn } from '../auth';
import Navbar from '../features/navbar/Navbar';

export default function Router () {
  return (
    <HashRouter>
      <Navbar />
      <Switch>
        <Route exact path="/">
          <Index />
        </Route>
        <Route path="/login">
          <Login />
        </Route>
        <PrivateRoute path="/todo">
          <Todo />
        </PrivateRoute>
      </Switch>
    </HashRouter>
  )
}

type IProps = {
  children: ReactNode,
  path: string,
}

function PrivateRoute({ children, ...rest }: IProps) {
  const jwt = useSelector(selectJwt);

  return (
    <Route
      {...rest}
      render={({ location }) =>
        isLoggedIn(jwt) ? (
          children
        ) : (
          <Redirect
            to={{
              pathname: "/login",
              state: { from: location }
            }}
          />
        )
      }
    />
  );
}