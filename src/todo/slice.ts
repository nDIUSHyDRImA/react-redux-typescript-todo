import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { randomBytes } from "crypto";
import { RootState } from "../app/store";

type OneTodo = {
  key: string;
  content: string;
  isCompleted: boolean;
};

type TodoState = {
  allTodo: OneTodo[],
};

const initialState: TodoState = {
  allTodo: [],
};

export const slice = createSlice({
  name: "todo",
  initialState,
  reducers: {
    add: (state, action: PayloadAction<string>) => {
      const N = 16;
      const key = randomBytes(N)
        .toString("Base64")
        .substring(0, N);
      state.allTodo.push({
        key,
        content: action.payload,
        isCompleted: false
      });
    },
    remove: (state, action: PayloadAction<string>) => {
      state.allTodo = state.allTodo.filter(oneTodo => oneTodo.key !== action.payload);
    },
    complete: (state, action: PayloadAction<string>) => {
      state.allTodo
        .filter(oneTodo => oneTodo.key === action.payload)
        .forEach(oneTodo => (oneTodo.isCompleted = !oneTodo.isCompleted));
    }
  }
});

export const { add, remove, complete } = slice.actions;

export const selectAllTodo = (state: RootState) => state.todo.allTodo;

export default slice.reducer;
