import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { add, selectAllTodo, complete, remove } from "./slice";
import styles from './Todo.module.css';

export default function Todo() {
  const [newTodo, setNewTodo] = useState("");
  const allTodo = useSelector(selectAllTodo);
  const dispatch = useDispatch();

  const handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    dispatch(add(newTodo));
    setNewTodo("");
  };

  return (
    <>
      <form onSubmit={e => handleSubmit(e)}>
        <input
          type="text"
          className="input"
          value={newTodo}
          onChange={e => setNewTodo(e.currentTarget.value)}
        />
        <button type="submit" className="button is-primary">
          add
        </button>
      </form>
      <ul>
        {allTodo.map(oneTodo => (
          <li key={oneTodo.key}>
            <label>
              <input
                type="checkbox"
                checked={oneTodo.isCompleted}
                onChange={() => dispatch(complete(oneTodo.key))}
              />
              <span className={oneTodo.isCompleted ? styles.complete : ''}>{oneTodo.content}</span>
            </label>
            <button className="button" type="button" onClick={() => dispatch(remove(oneTodo.key))}>remove</button>
          </li>
        ))}
      </ul>
    </>
  );
}
