import React from 'react';
import { Link } from 'react-router-dom';
import { useSelector } from 'react-redux';
import { selectJwt } from './auth/slice';
import { isLoggedIn } from './auth';

export default function App2() {
  const jwt: string = useSelector(selectJwt);

  return (
    <>
    <p>Strongest Todo App</p>
    {isLoggedIn(jwt) ? 
      <Link to="/todo">Go to todo</Link> :
      <Link to="/login">Login</Link>
    }
    </>
  )
}