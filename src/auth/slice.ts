import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from '../app/store';

type authState = {
  jwt: string,
}

const initialState: authState = {
  jwt: localStorage.getItem('jwt') || '',
}

export const slice = createSlice({
  name: 'auth',
  initialState,
  reducers: {
    setAuthState: (state, action: PayloadAction<authState>) => {
      state.jwt = action.payload.jwt;
      localStorage.setItem('jwt', action.payload.jwt);
    },
    resetAuthState: state => {
      state.jwt = '';
      localStorage.removeItem('jwt');
    },
  }
})

export const { setAuthState, resetAuthState } = slice.actions;

export const selectJwt = (state: RootState) => state.auth.jwt;

export default slice.reducer;