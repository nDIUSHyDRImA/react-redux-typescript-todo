export const isLoggedIn = (jwt: string) => !!jwt;

export const correctUsername = (username: string) => username === 'foo';
export const correctPassword = (password: string) => password === 'bar';